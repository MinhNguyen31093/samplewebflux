package com.demo.project.config;

import io.netty.handler.logging.LogLevel;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.adapter.ForwardedHeaderTransformer;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public WebClient webClient() {
        HttpClient httpClient = HttpClient.create().wiretap("reactor.netty.http.client.HttpClient", LogLevel.DEBUG, AdvancedByteBufFormat.TEXTUAL);
        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .filter((clientRequest, exchangeFunction) -> {
                    if (clientRequest.url().getHost().equals("petstore3.swagger.io")) {
                        ClientRequest newClientRequest = ClientRequest.from(clientRequest)
                                .header("Token", UUID.randomUUID().toString())
                                .build();
                        return exchangeFunction.exchange(newClientRequest);
                    }
                    return exchangeFunction.exchange(clientRequest);
                })
                .build();
    }

    @Bean
    public OperationCustomizer customGlobalHeaders() {
        return (Operation operation, HandlerMethod handlerMethod) -> {
            List<Parameter> parameters = operation.getParameters();
            if (parameters == null) {
                parameters = new ArrayList();
            }
            parameters.add(new Parameter().in(ParameterIn.HEADER.toString()).schema(new StringSchema()).name("test").description("Test").required(true).example("test"));
            operation.setParameters(parameters);
            return operation;
        };
    }

    @Bean
    public ForwardedHeaderTransformer forwardedHeaderTransformer () {
        return new ForwardedHeaderTransformer();
    }
}