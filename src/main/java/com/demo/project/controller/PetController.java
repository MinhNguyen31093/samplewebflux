package com.demo.project.controller;

import com.demo.project.api.PetApi;
import com.demo.project.model.Pet;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@Tag(name = "Pet", description = "Pet")
public class PetController implements PetApi {

    @Autowired
    WebClient webClient;

    @Override
    public Mono<ResponseEntity<Pet>> postPet(Mono<Pet> pet, ServerWebExchange exchange) {
        return webClient.post().uri("https://petstore3.swagger.io/api/v3/pet")
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_VALUE)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE)
                .body(pet, Pet.class)
                .retrieve()
                .bodyToMono(Pet.class).map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Pet>> getPet(ServerWebExchange exchange) {
        return webClient.get().uri("https://petstore3.swagger.io/api/v3/pet/10")
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_VALUE)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE)
                .retrieve()
                .bodyToMono(Pet.class).map(ResponseEntity::ok);
    }
}
